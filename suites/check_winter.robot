*** Settings ***
Library    SeleniumLibrary
Resource    ../resources/weather_service_finland.resource
Suite Setup    Open browser    https://google.fi    headlessfirefox
Suite Teardown    Close browser

*** Tasks ***
Validate temperature in Helsinki is cold
    [Template]      Validate emperature in Helsinki is below
    10.5
    10.0
    9
    8
    7
    6
    5
    4
    3
    2
    1
    0

*** Keywords ***
Validate emperature in Helsinki is below
    [Arguments]    ${max_temperature}
    ${current_temperature}    Get current temperature from Helsinki
    Should Not Be True    ${max_temperature} < ${current_temperature}   Failure in Helsinki. Current temperature ${current_temperature} is above ${max_temperature}. Spring is coming.
