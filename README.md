# Project structure

In this workshop, we propose a project structure we have been working with for last couple of months. **:warning: Be aware that this structure is a proposal. If it does not fit your needs, you may fix it or drop it altogether.** Once we get a better idea, we will do the same :sunglasses:

# Mandatory folders

These folders contain configuration data mandatory for setting up the test environment and starting your robot tasks or tests.

## install
The `install` folder contains everything required for setting up the runtime environment. At least, a `requirements.txt` is necessary containing pip dependency for *robotframework*:

```
robotframework
```

In every python environment, your runtime can be set up by simply calling:

```sh
pip install -r install/requirements.txt --upgrade
```

### Use version ranges
Dependencies in your python environment are not fixed. Libraries like selenliumlibrary and robotframework itself are enhanced continously and release newer versions from time to time. **:warning: If you do not use fix versions, new releases might break your environment**. Luckily, pip understands version ranges:

```
robotframework>=3.1,<3.2
robotframework-seleniumlibrary>=4,<4.1
```

Now versions of your dependencies are limited to a certain range. Whenever you become aware of a new major version, follow this checklist:

* [ ] Create feature branch for testing a new version
* [ ] Change version range of your dependency containing the new major version
* [ ] Run your tests
* [ ] If tests are ok, merge branch
* [ ] If tests are not ok, fix it until it is ok.

## config

The `config` contains configurations required by your tests or tasks. You might want to create subfolders for different environment like `config/dev` and `config/prod`. 

**If you use yaml for configuration, you need to add `pyyaml` dependency to your python enviroment**. Maybe, just add it to your `requirements.txt` :bulb:

You can pass configuration files to robot execution with `--variablefile` option:

```
robot --variablefile config/dev/robot-conf.yaml MyTestSuite.robot
```

## run
The `run` folder contains scripts and shortcuts for running your suites. It may contain shell scripts, too, but it should at least contain some robot argumentsfiles making your life easier executing robot. For example, you could create an argument file for each enviroment, such as `arguments_dev.txt` and `arguments_prod.txt`. Content of such a file could look like this:

```
--variablefile config/dev/robot-conf.yaml 
-d logs 
-b console.logs
--flattenkeywords TAG:secret
--removekeywords TAG:secret
```

Pass this argumentfile to robot with the `--argumentfile` option:

```sh
robot --argumentfile run/arguments_dev.txt MyTestSuite.robot
```

# Optional folders

These folders contain your actual robot sources. For small projects (especially rpa projects), we only use the `resources` folder and leave the main robot file at root level. 

## resources

Folder containing *robot resource* files with common keywords shared between suites and test cases.

## tests

Folder contain tests that test your robot sources. While it seems odd at first, you may want to test robot tasks and robot tests as well, especially keywords. Those tests are located here in this folder.

## suites

Folder containing robot suites, either tests or tasks.

